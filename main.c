#include "instance.h"
#include "geometry.h"

int main() {
    int i, j;

    Instance *n = instance_create();

    /* classificacao de poligonos */
    for (i = 0; i < n->n_polygons; i++) 
        if (polygon_is_simple(n->polygon_buf[i]))
            if (polygon_is_convex(n->polygon_buf[i]))
                printf("%d simples e convexo\n", i+1);
            else printf("%d simples e nao convexo\n", i+1);
        else 
           printf("%d nao simples\n", i);

    /* pontos interiores */
    for (i = 0; i < n->n_polygons; i++) {
        printf("%d:", i+1);

        for (j = 0; j < n->n_points; j++)
            if (point_inside_polygon(n->point_buf[j], n->polygon_buf[i]))
                printf("%d ", j);

        putchar('\n');
    }
        
    instance_destroy(n);

    return 0;
}

