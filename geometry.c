#include "geometry.h"

/* Trab - Implementacao Ingenua */

void point_print(Point *p) {
    printf("%f %f\n", p->x, p->y);
}

void polygon_print(Polygon *n_gon) {
    int i;

    for (i = 0; i < n_gon->n_edges; i++)
        point_print(&n_gon->vertices[i]);
}

/* ------------ */

/* A biblioteca mais vagabunda possiel de AlgLinear */
/* n trata os problemas da representacao em ponto flutuante *//* Teoricamente, nao eh espaco vetorial... */

void vector_sum(Point *p1, Point *p2, Point *v0) { /* v0 = p1 + p2 */
    v0->x = p1->x + p2->x;
    v0->y = p1->y + p2->y;
}

void vector_sum_inverse(Point *p1, Point *p2, Point *v0) { /* v0 = p1 + (-p2) */
    v0->x = p1->x - p2->x;
    v0->y = p1->y - p2->y;
}

float vector_determinant(Point *v0, Point *v1) {
    return v0->x * v1->y - v1->x * v0->y;
}


/* ------------ */

int polygon_is_convex(Polygon *n_gon) { /* Poligono precisa ser simples *//* n + 1 */
    int count, flag = 1;
    Point vectors[2]; /* Evitar de recalcular os vetores */

    vector_sum_inverse(&n_gon->vertices[1], &n_gon->vertices[0], &vectors[0]); /* Calculo primeiro vetor */
    
    for (count = 2; count < n_gon->n_edges + 2; count++) { /* Olhar os pontos (0, 1, 2) .. (2, 0, 1) */
        vector_sum_inverse(&n_gon->vertices[count % n_gon->n_edges], &n_gon->vertices[(count-1) % n_gon->n_edges], &vectors[flag]); /* Buffer Circular */
        
        if (vector_determinant(&vectors[!flag], &vectors[flag]) < 0) return 0;

        flag ^= 1; 
    }

    return 1;
}

/* --- */

/* LineSegment = Point buf[4] */

Point line_seg[4];

void point_segment_set(Point *line_seg, int i, Polygon *n_gon) {
    int count;

    for (count = 0; count < 2; count++)
        *(line_seg++) = n_gon->vertices[i++ % n_gon->n_edges];     
}

int point_segment_intersect(Point *line_seg) { /* Duvida : (*line_seg)[4] */
    Point a, b, c;
    float det, s, t;

    vector_sum_inverse(&line_seg[1], &line_seg[0], &a); /* v2 - v1 */
    vector_sum_inverse(&line_seg[2], &line_seg[3], &b); /* v3 - v4 == -(v4 - v3)*/
    vector_sum_inverse(&line_seg[2], &line_seg[0], &c); /* v3 - v1 */

    det = vector_determinant(&a, &b);
    s = vector_determinant(&c, &b);
    t = vector_determinant(&a, &c);

    if (det != 0 && (0 <= s/det && s/det <= 1 ) && (0 <= t/det && t/det <= 1))
        return 1;

    return 0;
}

int polygon_is_simple(Polygon *n_gon) { /* Binom{n}{2} */
    int i, j, count = 0;
    Point line_seg[4];

    for (i = 0; i < n_gon->n_edges-1; i++) {
        point_segment_set(&line_seg[0], i, n_gon);

        for (j = i+1; j < n_gon->n_edges; j++) {
            point_segment_set(&line_seg[2], j, n_gon);

            if (point_segment_intersect(&line_seg[0])) 
                ++count;
        }
    }

    return (count == n_gon->n_edges);
}

/* --- */

int point_inside_polygon(Point *p, Polygon *n_gon) {
    return 0;
}

/* ------------ */


