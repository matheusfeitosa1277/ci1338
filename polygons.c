#include "polygons.h"

/* Point */

Point *point_create(float x, float y) {
    Point *p = malloc(sizeof(Point));

    p->x = x;
    p->y = y;

    return p;
}

void point_destroy(Point *p) {
    free(p);
}

/* Polygon */

Polygon *polygon_create(int n_edges) {
    Polygon *n_gon = malloc(sizeof(Polygon) + sizeof(Point) * n_edges);
    n_gon->n_edges = n_edges;

    return n_gon;
}

void polygon_destroy(Polygon *n_gon) {
    free(n_gon);
}

