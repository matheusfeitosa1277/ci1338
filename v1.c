/* Programa mais porco impossivel */

#include <stdio.h>
#include <stdlib.h>

typedef struct {
    float x, y;
} point;

typedef struct {
    unsigned int n_edges;
    point vertices[0];
} polygon;

polygon *polygon_create(unsigned int n_edges) {
    polygon *poly = malloc(sizeof(polygon) + sizeof(point) * n_edges);
    poly->n_edges = n_edges;

    return poly;
}

void polygon_destroy(polygon *poly) {
    free(poly);
}

int main() {
    int aux;
    //int size = 5;
    //point polygon[] = {{1, 4}, {5, 4}, {15, 4}, {15, 20}, {1, 20}};
    int size = 4;
    //point polygon[] = {{0, 1}, {-1, 0}, {0, -1}, {1, 0}};
    point polygon[] = {{-1, -1}, {1, -1}, {1, 1}, {-1, 1}};

    point aux1, aux2;
    float det;

    for (aux = 2; aux < size; aux++) {

        aux1.x = polygon[aux-1].x  - polygon[aux-2].x;  
        aux1.y = polygon[aux-1].y  - polygon[aux-2].y;   

        aux2.x = polygon[aux].x  - polygon[aux-1].x;  
        aux2.y = polygon[aux].y  - polygon[aux-1].y;   

        printf(" | %f %f\n", aux1.x, aux1.y);
        printf(" | %f %f\n", aux2.x, aux2.y);

        det =  aux1.x * aux2.y - aux2.x * aux1.y;

        printf("Det: %f\n\n", det);
    }

    /* ----------------- */

    aux1.x = polygon[size-1].x  - polygon[size-2].x;  
    aux1.y = polygon[size-1].y  - polygon[size-2].y;   

    aux2.x = polygon[0].x  - polygon[size-1].x;  
    aux2.y = polygon[0].y  - polygon[size-1].y;   

    printf(" | %f %f\n", aux1.x, aux1.y);
    printf(" | %f %f\n", aux2.x, aux2.y);

    printf("Det: %f\n\n", det);
    
    /* ----------------- */
    aux1.x = polygon[0].x  - polygon[size-1].x;  
    aux1.y = polygon[0].y  - polygon[size-1].y;   

    aux2.x = polygon[1].x  - polygon[0].x;  
    aux2.y = polygon[1].y  - polygon[0].y;   

    printf(" | %f %f\n", aux1.x, aux1.y);
    printf(" | %f %f\n", aux2.x, aux2.y);

    printf("Det: %f\n\n", det);


    return 0;
}
