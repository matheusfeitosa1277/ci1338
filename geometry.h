#ifndef GEOMETRY_H
#define GEOMETRY_H

#include "polygons.h"

int polygon_is_convex(Polygon *n_gon);

int polygon_is_simple(Polygon *n_gon);

int point_inside_polygon(Point *p, Polygon *n_gon);

#endif

