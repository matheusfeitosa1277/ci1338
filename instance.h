#ifndef INSTANCE_H 
#define INSTANCE_H 

#define LINESIZE 1024

#include "polygons.h"

typedef struct {
   int n_polygons, n_points; 
   Polygon **polygon_buf;
   Point **point_buf;
} Instance;

Instance *instance_create();

void instance_destroy(Instance *n);

#endif

