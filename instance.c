#include "instance.h"

Instance *instance_create() {
    int i, j, count, n_edges;
    float x, y;
    char line_buf[LINESIZE];

    Instance *n = malloc(sizeof(Instance));

    /* Leitura n pontos e poligonos */
    fgets(line_buf, LINESIZE, stdin);
    sscanf(line_buf, "%d %d", &n->n_polygons, &n->n_points);

    n->polygon_buf = malloc(sizeof(Polygon*) * n->n_polygons);
    n->point_buf = malloc(sizeof(Point*) * n->n_points);

    /* Leitura dos poligonos */
    i = 0;
    count = n->n_polygons;
    while (count--) { 
        fgets(line_buf, LINESIZE, stdin);
        sscanf(line_buf, "%d", &n_edges);

        n->polygon_buf[i] = polygon_create(n_edges);

        j = 0;
        while (n_edges--) {
            fgets(line_buf, LINESIZE, stdin);
            sscanf(line_buf, "%f %f", &x, &y);

            n->polygon_buf[i]->vertices[j].x = x;
            n->polygon_buf[i]->vertices[j++].y = y;
        }

        i++;
    }

    /* Leitura dos pontos */
    i = 0;
    count = n->n_points;
    while (count--) {
            fgets(line_buf, LINESIZE, stdin);
            sscanf(line_buf, "%f %f", &x, &y);

            n->point_buf[i] = point_create(x, y);

            i++;
    }

    return n;
}

void instance_destroy(Instance *n) {
    int i;

    for (i = 0; i < n->n_polygons; i++)
        polygon_destroy(n->polygon_buf[i]);

    free(n->polygon_buf);
        
    for (i = 0; i < n->n_points; i++) 
        point_destroy(n->point_buf[i]);

    free(n->point_buf);

    free(n);
}

