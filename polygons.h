#ifndef POLYGONS_H
#define POLYGONS_H

#include <stdio.h>
#include <stdlib.h>

typedef struct {
    float x, y;
} Point;

Point *point_create(float x, float y);

void point_destroy(Point *p);

/* ------------ */

typedef struct {
    int n_edges;
    Point vertices[0];
} Polygon;

Polygon *polygon_create(int n_edges);

void polygon_destroy(Polygon *n_gon);

#endif

